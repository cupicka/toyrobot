package com.bojanac.robot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RobotApplication {

    private static final Logger log = LoggerFactory.getLogger(RobotApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(RobotApplication.class, args);
    }
}
