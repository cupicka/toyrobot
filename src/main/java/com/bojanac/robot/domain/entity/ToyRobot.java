package com.bojanac.robot.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
public class ToyRobot {
    @Id
    @GeneratedValue
    private Long id;

    private Position position;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Transient
    private List<Command> commands;

    public ToyRobot(Position position) {
        this.position = position;
    }

    public ToyRobot(Position position, List<Command> commands) {
        this.position = position;
        this.commands = commands;
    }
}
