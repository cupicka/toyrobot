package com.bojanac.robot.domain.entity;

import com.bojanac.robot.controller.exception.ApiException;

public enum Command {

    MOVE {
        @Override
        public void placeChange(Position position) {
            if (position.getF() == F.NORTH || position.getF() == F.SOUTH) {
                position.setY(position.getY() + 1);
            } else {
                position.setX(position.getX() + 1);
            }
        }
    }, RIGHT {
        @Override
        public void placeChange(Position position) {
            switch (position.getF()) {
                case NORTH:
                    position.setF(F.EAST);
                    break;
                case EAST:
                    position.setF(F.SOUTH);
                    break;
                case WEST:
                    position.setF(F.NORTH);
                    break;
                case SOUTH:
                    position.setF(F.WEST);
                    break;
                default:
                    throw new ApiException(ApiException.ApiExceptionType.TOYROBOT_WRONG_ORIENTATION);
            }
        }
    }, LEFT {
        @Override
        public void placeChange(Position position) {
            switch (position.getF()) {
                case NORTH:
                    position.setF(F.WEST);
                    break;
                case EAST:
                    position.setF(F.NORTH);
                    break;
                case WEST:
                    position.setF(F.SOUTH);
                    break;
                case SOUTH:
                    position.setF(F.EAST);
                    break;
                default:
                    throw new ApiException(ApiException.ApiExceptionType.TOYROBOT_WRONG_ORIENTATION);

            }
        }
    };

    public abstract void placeChange(Position position);

}
