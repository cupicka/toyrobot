package com.bojanac.robot.controller.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ApiException.class, MethodArgumentTypeMismatchException.class})
    public ResponseEntity<Object> handleException(Exception ex) {
        ApiException apiEx = null;
        if (ex instanceof MethodArgumentTypeMismatchException) {
            apiEx = new ApiException(ApiException.ApiExceptionType.ERROR_INTERNAL);
        } else if (ex instanceof ApiException) {
            apiEx = (ApiException) ex;
        }

        return new ResponseEntity<>(apiEx.getType().getExceptionMessage(), apiEx.getType().getStatusCode());
    }
}
