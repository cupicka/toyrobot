package com.bojanac.robot.controller;

import com.bojanac.robot.controller.exception.ToyRobotException;
import com.bojanac.robot.domain.entity.ToyRobot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ToyRobotResourceAssembler implements ResourceAssembler<ToyRobot, Resource<ToyRobot>> {

    private static final Logger log = LoggerFactory.getLogger(ToyRobotResourceAssembler.class);

    @Override
    public Resource<ToyRobot> toResource(ToyRobot toyRobot) {
        Link linkReport = null;
        try {
            linkReport = linkTo(methodOn(ToyRobotController.class).report(toyRobot.getId())).withSelfRel();
        } catch (ToyRobotException e) {
            log.error(e.getLocalizedMessage());
        }
        Link linkAllToyRobots = linkTo(methodOn(ToyRobotController.class).allToyRobots()).withRel("toyrobots");
        return new Resource<>(toyRobot,
                linkReport, linkAllToyRobots);

    }
}
