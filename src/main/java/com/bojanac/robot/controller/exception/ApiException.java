package com.bojanac.robot.controller.exception;

import org.springframework.http.HttpStatus;

public class ApiException extends RuntimeException {

    private ApiExceptionType type;

    public ApiException(ApiExceptionType ex) {
        this.type = ex;
    }

    public ApiExceptionType getType() {
        return type;
    }

    public enum ApiExceptionType {

        ERROR_INTERNAL(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error!"),
        TOYROBOT_NOT_ON_TABLE(HttpStatus.PRECONDITION_FAILED, "The toyrobot is not on the table!"),
        TOYROBOT_MUST_NOT_FALL_OFF(HttpStatus.CONFLICT, "The toyrobot is not on the table!"),
        TOYROBOT_PLACEMENT_IS_NOT_CORRECT(HttpStatus.BAD_REQUEST, "Toyrobot placment is not correct, please check values. Rang is from 0 to 5."),
        TOYROBOT_WRONG_ORIENTATION(HttpStatus.BAD_REQUEST, "Toyrobot orientation is misspelled.");


        private HttpStatus statusCode;
        private String exceptionMessage;

        ApiExceptionType(HttpStatus statusCode, String errorMessage) {
            this.statusCode = statusCode;
            this.exceptionMessage = errorMessage;
        }

        public HttpStatus getStatusCode() {
            return statusCode;
        }

        public String getExceptionMessage() {
            return exceptionMessage;
        }

    }
}
