package com.bojanac.robot;

import com.bojanac.robot.controller.ToyRobotController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ToyRobotControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private ToyRobotController toyRobotController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(toyRobotController)
                .build();
    }

    @Test(expected = NestedServletException.class)
    public void testNotExistingId() throws Exception {


        mockMvc.perform(get("/toyrobots/6")).andExpect(status().isBadRequest());
    }


}
