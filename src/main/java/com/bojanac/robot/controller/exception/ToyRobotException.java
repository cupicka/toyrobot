package com.bojanac.robot.controller.exception;

public class ToyRobotException extends Exception {
    public ToyRobotException(String message) {
        super(message);
    }
}
