package com.bojanac.robot.domain.entity;

import com.bojanac.robot.controller.exception.ApiException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;


@Getter
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Position {

    private int x;

    private int y;

    @Enumerated(EnumType.STRING)
    private F f;

    public void setX(int x) {
        if (x < 0 || x > 5)
            throw new ApiException(ApiException.ApiExceptionType.TOYROBOT_PLACEMENT_IS_NOT_CORRECT);
        this.x = x;
    }

    public void setY(int y) {
        if (y < 0 || y > 5)
            throw new ApiException(ApiException.ApiExceptionType.TOYROBOT_PLACEMENT_IS_NOT_CORRECT);
        this.y = y;
    }

    public void setF(F f) {
        this.f = f;
    }
}