package com.bojanac.robot.domain.repository;

import com.bojanac.robot.domain.entity.ToyRobot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToyRobotRepository extends JpaRepository<ToyRobot, Long> {

}
