package com.bojanac.robot;

import com.bojanac.robot.controller.exception.ApiException;
import com.bojanac.robot.domain.entity.F;
import com.bojanac.robot.domain.entity.Position;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class PositionTest {


    @Test
    public void testInitialPlacmentUpperBound() {

        assertThrows(ApiException.class, () -> new Position().setX(6));

    }


    @Test
    public void testInitialPlacmentLowerBound() {

        assertThrows(ApiException.class, () -> new Position().setY(-8));

    }

    @Test
    public void testInitialPlacmentOrientation() {

        assertThrows(IllegalArgumentException.class, () -> new Position().setF(F.valueOf("NWSE")));

    }
}
