package com.bojanac.robot.controller;

import com.bojanac.robot.controller.exception.ToyRobotException;
import com.bojanac.robot.domain.entity.Command;
import com.bojanac.robot.domain.entity.Position;
import com.bojanac.robot.domain.entity.ToyRobot;
import com.bojanac.robot.domain.repository.ToyRobotRepository;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class ToyRobotController {


    private final ToyRobotRepository repository;
    private final ToyRobotResourceAssembler assembler;

    public ToyRobotController(ToyRobotRepository repository, ToyRobotResourceAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/toyrobots")
    Resources<Resource<ToyRobot>> allToyRobots() {

        List<Resource<ToyRobot>> toyrobots = repository.findAll().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(toyrobots,
                linkTo(methodOn(ToyRobotController.class).allToyRobots()).withSelfRel());
    }

    @PostMapping("/toyrobots")
    Resource<ToyRobot> initiallyPlace(@RequestBody ToyRobot toyRobot) {
        repository.save(toyRobot);
        return assembler.toResource(toyRobot);
    }

    @GetMapping("/toyrobots/{id}")
    Resource<ToyRobot> report(@PathVariable Long id) throws ToyRobotException {

        ToyRobot toyRobot = repository.findById(id)
                .orElseThrow(() -> new ToyRobotException("The toyrobot is not on the table!"));
        return assembler.toResource(toyRobot);
    }

    @PutMapping("/toyrobots/{id}")
    Resource<ToyRobot> moveRobot(@RequestBody ToyRobot toyRobotMoved, @PathVariable long id) throws ToyRobotException {

        ToyRobot toyRobotR = repository.findById(id)
                .map(toyRobot -> {
                    toyRobot.setPosition(move(toyRobotMoved.getPosition(), toyRobotMoved.getCommands()));
                    return repository.save(toyRobot);
                }).orElseThrow(() -> new ToyRobotException("The toyrobot must not fall off the table!"));
        return assembler.toResource(toyRobotR);
    }

    private Position move(Position position, List<Command> commands) {
        for (Command command : commands) {
            if (command == Command.MOVE) {
                Command.MOVE.placeChange(position);
            } else if (command == Command.LEFT) {
                Command.LEFT.placeChange(position);
            } else {
                Command.RIGHT.placeChange(position);
            }
        }
        return position;

    }
}
